package ru.t1.avfilippov.tm.unit.controller;

import lombok.SneakyThrows;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;
import ru.t1.avfilippov.tm.api.service.TaskDTOService;
import ru.t1.avfilippov.tm.entity.dto.TaskDto;
import ru.t1.avfilippov.tm.marker.UnitCategory;
import ru.t1.avfilippov.tm.util.UserUtil;

import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Transactional
@SpringBootTest
@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
@Category(UnitCategory.class)
public class TaskControllerTest {

    private final TaskDto task1 = new TaskDto("Test TaskDto 1");

    private final TaskDto task2 = new TaskDto("Test TaskDto 2");

    private final TaskDto task3 = new TaskDto("Test TaskDto 3");

    private final TaskDto task4 = new TaskDto("Test TaskDto 4");

    @Autowired
    private TaskDTOService taskDtoService;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private WebApplicationContext wac;

    private MockMvc mockMvc;

    @Before
    public void init() {
        mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
        final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken("test", "test");
        final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        task1.setUserId(UserUtil.getUserId());
        task2.setUserId(UserUtil.getUserId());
        taskDtoService.save(task1);
        taskDtoService.save(task2);
    }

    @After
    public void clean() {
        taskDtoService.clear(UserUtil.getUserId());
    }

    @Test
    @SneakyThrows
    @Category(UnitCategory.class)
    public void findAllTest() {
        final String url = "/tasks";
        mockMvc.perform(MockMvcRequestBuilders.get(url))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    @SneakyThrows
    @Category(UnitCategory.class)
    public void createTest() {
        final String url = "/task/create";
        final List<TaskDto> taskDtoBeforeList = taskDtoService.findAll(UserUtil.getUserId());
        mockMvc.perform(MockMvcRequestBuilders.get(url))
                .andDo(print())
                .andExpect(status().is3xxRedirection());
        final List<TaskDto> taskDtoList = taskDtoService.findAll(UserUtil.getUserId());
        Assert.assertNotNull(taskDtoList);
        Assert.assertEquals(taskDtoBeforeList.size() + 1, taskDtoList.size());
    }

    @Test
    @SneakyThrows
    @Category(UnitCategory.class)
    public void deleteTest() {
        final String url = "/task/delete/" + task1.getId();
        mockMvc.perform(MockMvcRequestBuilders.get(url))
                .andDo(print())
                .andExpect(status().is3xxRedirection());
        Assert.assertNull(taskDtoService.findByUserIdAndId(UserUtil.getUserId(), task1.getUserId()));
    }

    @Test
    @SneakyThrows
    @Category(UnitCategory.class)
    public void editTest() {
        final String url = "/task/edit/" + task2.getId();
        mockMvc.perform(MockMvcRequestBuilders.get(url))
                .andDo(print())
                .andExpect(status().is2xxSuccessful());
    }

}
