package ru.t1.avfilippov.tm.integration;

import org.junit.*;
import org.junit.experimental.categories.Category;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;
import ru.t1.avfilippov.tm.entity.dto.ProjectDto;
import ru.t1.avfilippov.tm.entity.dto.Result;
import ru.t1.avfilippov.tm.marker.IntegrationCategory;

import java.net.HttpCookie;
import java.util.Arrays;
import java.util.List;

@Category(IntegrationCategory.class)
public class ProjectsRestEndpointTest {

    private static final String BASE_URL = "http://localhost:8080/api/projects/";

    private static final String PROJECT_URL = "http://localhost:8080/api/projects/";

    private static final HttpHeaders HEADER = new HttpHeaders();

    private final ProjectDto projectDto1 = new ProjectDto("Test 1");

    private final ProjectDto projectDto2 = new ProjectDto("Test 2");

    private final ProjectDto projectDto3 = new ProjectDto("Test 3");

    private final ProjectDto projectDto4 = new ProjectDto("Test 4");

    private final List<ProjectDto> projectDtoList1 = Arrays.asList(projectDto1, projectDto2);

    private final List<ProjectDto> projectDtoList2 = Arrays.asList(projectDto3, projectDto4);

    @BeforeClass
    public static void beforeClass() {
        final RestTemplate restTemplate = new RestTemplate();
        final String url = "http://localhost:8080/api/auth/login?username=test&password=test";
        final ResponseEntity<Result> response =
                restTemplate.getForEntity(url, Result.class);
        Assert.assertEquals(200, response.getStatusCodeValue());
        Assert.assertNotNull(response.getBody());
        Assert.assertTrue(response.getBody().getSuccess());
        HttpHeaders headersResponse = response.getHeaders();
        List<HttpCookie> cookies = HttpCookie.parse(
                headersResponse.getFirst(HttpHeaders.SET_COOKIE)
        );
        final String sessionId = cookies.stream()
                .filter(
                        item -> "JSESSIONID".equals(item.getName())
                ).findFirst().get().getValue();
        Assert.assertNotNull(sessionId);
        HEADER.put(HttpHeaders.COOKIE, Arrays.asList("JSESSIONID=" + sessionId));
        HEADER.setContentType(MediaType.APPLICATION_JSON);
    }

    private static ResponseEntity<List> sendRequestList(final String url, final HttpMethod method, final HttpEntity httpEntity) {
        final RestTemplate restTemplate = new RestTemplate();
        return restTemplate.exchange(url, method, httpEntity, List.class);
    }

    private static ResponseEntity<ProjectDto> sendRequest(final String url, final HttpMethod method, final HttpEntity httpEntity) {
        final RestTemplate restTemplate = new RestTemplate();
        return restTemplate.exchange(url, method, httpEntity, ProjectDto.class);
    }

    @AfterClass
    public static void logout() {
        final RestTemplate restTemplate = new RestTemplate();
        final String logoutUrl = "http://localhost:8080/api/auth/logout";
        sendRequest(logoutUrl, HttpMethod.GET, new HttpEntity<>(HEADER));
    }

    @Before
    public void initTest() {
        final String url = BASE_URL + "save/";
        sendRequestList(url, HttpMethod.POST, new HttpEntity<>(projectDtoList1, HEADER));
    }

    @After
    public void clean() {
        final String url = BASE_URL + "removeAll/";
        sendRequestList(url, HttpMethod.DELETE, new HttpEntity<>(HEADER));
    }

    @Test
    @Category(IntegrationCategory.class)
    public void testAdd() {
        final String url = BASE_URL + "save/";
        sendRequestList(url, HttpMethod.POST, new HttpEntity<>(projectDtoList2, HEADER));
        final String findUrl = PROJECT_URL + "findById/" + projectDto4.getId();
        Assert.assertNotNull(sendRequest(findUrl, HttpMethod.GET, new HttpEntity<>(HEADER)).getBody());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void findAll() {
        final String url = BASE_URL + "findAll/";
        Assert.assertEquals(3, sendRequestList(url, HttpMethod.GET, new HttpEntity<>(HEADER)).getBody().size());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void remove() {
        final String url = BASE_URL + "remove/";
        sendRequestList(url, HttpMethod.POST, new HttpEntity<>(projectDtoList1, HEADER));
        final String findUrl = BASE_URL + "findAll/";
        Assert.assertEquals(0, sendRequestList(findUrl, HttpMethod.GET, new HttpEntity<>(HEADER)).getBody().size());
    }

}
