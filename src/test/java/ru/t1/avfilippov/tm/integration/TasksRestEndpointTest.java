package ru.t1.avfilippov.tm.integration;

import org.junit.*;
import org.junit.experimental.categories.Category;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;
import ru.t1.avfilippov.tm.entity.dto.Result;
import ru.t1.avfilippov.tm.entity.dto.TaskDto;
import ru.t1.avfilippov.tm.marker.IntegrationCategory;

import java.net.HttpCookie;
import java.util.Arrays;
import java.util.List;

@Category(IntegrationCategory.class)
public class TasksRestEndpointTest {

    private static final String BASE_URL = "http://localhost:8080/api/tasks/";

    private static final String TASK_URL = "http://localhost:8080/api/tasks/";

    private static final HttpHeaders HEADER = new HttpHeaders();

    private final TaskDto taskDto1 = new TaskDto("Test 1");

    private final TaskDto taskDto2 = new TaskDto("Test 2");

    private final TaskDto taskDto3 = new TaskDto("Test 3");

    private final TaskDto taskDto4 = new TaskDto("Test 4");

    private final List<TaskDto> taskDtoList1 = Arrays.asList(taskDto1, taskDto2);

    private final List<TaskDto> taskDtoList2 = Arrays.asList(taskDto3, taskDto4);

    @BeforeClass
    public static void beforeClass() {
        final RestTemplate restTemplate = new RestTemplate();
        final String url = "http://localhost:8080/api/auth/login?username=test&password=test";
        final ResponseEntity<Result> response =
                restTemplate.getForEntity(url, Result.class);
        Assert.assertEquals(200, response.getStatusCodeValue());
        Assert.assertNotNull(response.getBody());
        Assert.assertTrue(response.getBody().getSuccess());
        HttpHeaders headersResponse = response.getHeaders();
        List<HttpCookie> cookies = HttpCookie.parse(
                headersResponse.getFirst(HttpHeaders.SET_COOKIE)
        );
        final String sessionId = cookies.stream()
                .filter(
                        item -> "JSESSIONID".equals(item.getName())
                ).findFirst().get().getValue();
        Assert.assertNotNull(sessionId);
        HEADER.put(HttpHeaders.COOKIE, Arrays.asList("JSESSIONID=" + sessionId));
        HEADER.setContentType(MediaType.APPLICATION_JSON);
    }

    private static ResponseEntity<List> sendRequestList(final String url, final HttpMethod method, final HttpEntity httpEntity) {
        final RestTemplate restTemplate = new RestTemplate();
        return restTemplate.exchange(url, method, httpEntity, List.class);
    }

    private static ResponseEntity<TaskDto> sendRequest(final String url, final HttpMethod method, final HttpEntity httpEntity) {
        final RestTemplate restTemplate = new RestTemplate();
        return restTemplate.exchange(url, method, httpEntity, TaskDto.class);
    }

    @AfterClass
    public static void logout() {
        final RestTemplate restTemplate = new RestTemplate();
        final String logoutUrl = "http://localhost:8080/api/auth/logout";
        sendRequest(logoutUrl, HttpMethod.GET, new HttpEntity<>(HEADER));
    }

    @Before
    public void initTest() {
        final String url = BASE_URL + "save/";
        sendRequestList(url, HttpMethod.POST, new HttpEntity<>(taskDtoList1, HEADER));
    }

    @After
    public void clean() {
        final String url = BASE_URL + "removeAll/";
        sendRequestList(url, HttpMethod.DELETE, new HttpEntity<>(HEADER));
    }

    @Test
    @Category(IntegrationCategory.class)
    public void testAdd() {
        final String url = BASE_URL + "save/";
        sendRequestList(url, HttpMethod.POST, new HttpEntity<>(taskDtoList2, HEADER));
        final String findUrl = TASK_URL + "findById/" + taskDto4.getId();
        Assert.assertNotNull(sendRequest(findUrl, HttpMethod.GET, new HttpEntity<>(HEADER)).getBody());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void findAll() {
        final String url = BASE_URL + "findAll/";
        Assert.assertEquals(3, sendRequestList(url, HttpMethod.GET, new HttpEntity<>(HEADER)).getBody().size());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void remove() {
        final String url = BASE_URL + "remove/";
        sendRequestList(url, HttpMethod.POST, new HttpEntity<>(taskDtoList1, HEADER));
        final String findUrl = BASE_URL + "findAll/";
        Assert.assertEquals(0, sendRequestList(findUrl, HttpMethod.GET, new HttpEntity<>(HEADER)).getBody().size());
    }

}
