package ru.t1.avfilippov.tm.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.avfilippov.tm.api.service.UserDTOService;
import ru.t1.avfilippov.tm.entity.dto.UserDto;
import ru.t1.avfilippov.tm.repository.UserDtoRepository;

import java.util.List;

@Service
public class UserDtoServiceImpl implements UserDTOService {

    @Autowired
    private UserDtoRepository userRepository;

    @Override
    public List<UserDto> findAll() {
        return userRepository.findAll();
    }

    @Override
    public UserDto save(final UserDto user) {
        return userRepository.save(user);
    }

    @Override
    public UserDto findById(final String id) {
        return userRepository.findById(id).orElse(null);
    }

    @Override
    public boolean existsById(final String id) {
        return userRepository.existsById(id);
    }

    @Override
    public UserDto save() {
        final UserDto user = new UserDto();
        return userRepository.save(user);
    }

    @Override
    public long count() {
        return userRepository.count();
    }

    @Override
    public void deleteById(final String id) {
        userRepository.deleteById(id);
    }

    @Override
    public void delete(final UserDto user) {
        userRepository.delete(user);
    }

    @Override
    public void deleteAll(final List<UserDto> usersDto) {
        userRepository.deleteAll(usersDto);
    }

    @Override
    public void clear() {
        userRepository.deleteAll();
    }

}
