package ru.t1.avfilippov.tm.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.t1.avfilippov.tm.entity.dto.CustomUser;
import ru.t1.avfilippov.tm.entity.model.Role;
import ru.t1.avfilippov.tm.entity.model.User;
import ru.t1.avfilippov.tm.enumerated.RoleType;
import ru.t1.avfilippov.tm.repository.UserRepository;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service("userDetailsService")
public class UserDetailsServiceBean implements UserDetailsService {

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) {
        final User user = userRepository.findByLogin(username);
        if (user == null) throw new UsernameNotFoundException(username);
        final List<Role> userRoles = user.getRoles();
        final List<String> roles = new ArrayList<>();
        for (final Role role : userRoles) roles.add(role.toString());
        return new CustomUser(org.springframework.security.core.userdetails.User
                .withUsername(user.getLogin())
                .password(user.getPasswordHash())
                .roles(roles.toArray(new String[]{}))
                .build()).withUserId(user.getId());
    }

    @PostConstruct
    private void init() {
        initUser("admin", "admin", RoleType.ADMINISTRATOR);
        initUser("test", "test", RoleType.USER);
    }

    public User findByLogin(final String login) {
        final User user = userRepository.findByLogin(login);
        return user;
    }

    private void initUser(final String login, final String pass, final RoleType type) {
        final User user = userRepository.findByLogin(login);
        if ((user != null)) return;
        createUser(login, pass, type);
    }

    private void createUser(final String login, final String pass, final RoleType type) {
        if (login == null || login.isEmpty()) return;
        if (pass == null || pass.isEmpty()) return;
        final String passwordHash = passwordEncoder.encode(pass);
        final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(passwordHash);
        final Role role = new Role();
        role.setUser(user);
        role.setRoleType(type);
        user.setRoles(Collections.singletonList(role));
        userRepository.save(user);
    }

}
