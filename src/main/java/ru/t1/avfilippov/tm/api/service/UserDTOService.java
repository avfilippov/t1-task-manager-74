package ru.t1.avfilippov.tm.api.service;

import ru.t1.avfilippov.tm.entity.dto.UserDto;

import java.util.List;

public interface UserDTOService {

    List<UserDto> findAll();

    UserDto save(UserDto user);

    UserDto findById(String id);

    boolean existsById(String id);

    UserDto save();

    long count();

    void deleteById(String id);

    void delete(UserDto project);

    void deleteAll(List<UserDto> projectsDto);

    void clear();

}
